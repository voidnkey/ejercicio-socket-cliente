package com.voidkey.cliente;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class Main {

	public static void main(String[] args) throws UnknownHostException, IOException, ClassNotFoundException {

		InetAddress host = InetAddress.getLocalHost();
		Socket socket = null;

		socket = new Socket(host.getHostName(), 5555);
		socket.setKeepAlive(true);
		ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
		ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
		
		//oos.writeObject("apagar");
		oos.writeObject("VOID-KEY.com");
		
		String mensaje = (String) ois.readObject();
		System.out.println(mensaje);
	}

}
